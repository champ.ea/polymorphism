package com.champ.test;

public class Impl {

    public static void main(String[] args) {
        Salary s = new Salary("Mohd Mohtashim", "Ambehta, UP", 3, 3600.00);
        Employee e = new Salary("John Adams", "Boston, MA", 2, 2400.00);
        Employee y = new Employee("Edward", "Makati", 4);
        System.out.println("Call mailCheck using Salary reference --");
        s.computePay();
        s.mailCheck();
        System.out.println("Call mailCheck using Employee reference--");
        e.mailCheck();
        y.mailCheck();
    }

}
